extern crate time;
use time::PreciseTime;
use std::{process,env};

fn main() {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        1 => { 
            println!("No arguments provided. What position in the fibonacci sequence?");
        },
        2 => {
            let start = PreciseTime::now();
            let arg: u128 = match args[1].parse() {
                Ok(num) => fibo(num, 1, 1, 0),
                Err(_) => {
                    eprintln!("Error: Argument is not an integer!");
                    process::exit(1);
            }
            };
            let end = PreciseTime::now();
            println!("{}", arg);
            println!("{:?} seconds", start.to(end));

        },
        _ => {
            println!("This program only accepts a single argument");
            process::exit(1);
        }
    };
}

fn fibo(n: u128, i: u128, curr: u128, prev: u128) -> u128 {
  if i >= n {
    return curr;
  }

  return fibo(n, i + 1, curr + prev, curr);
}
