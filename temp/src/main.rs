use std::io;

fn main() {
    println!("Converting from Fahrenheit (f) or Celsius (c)?");
    println!("Enter your selection: [f|c]:");

    let mut units = String::new();
    loop {
        io::stdin()
            .read_line(&mut units)
            .expect("Failed to read line");

        match units.trim() {
            "f" | "c" => break,
            _ => println!("Not f or c")
        };
    }

    let mut temp = String::new();
    loop {

        println!("Enter value to convert:");
        io::stdin()
            .read_line(&mut temp)
            .expect("Failed to read line");

        let temp: f32 = match temp.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match units.trim() {
            "f" => println!("{}F is {}C", temp, f_to_c(temp)),
            "c" => println!("{}C is {}F", temp, c_to_f(temp)),
            _ => println!("Unknown unit type: {}", units)
    }
        break
    }
}

fn f_to_c(i: f32) -> f32 {
    (i - 32.0) * 0.55
}

fn c_to_f(i: f32) -> f32 {
    (i * 1.8) + 32.0
}
